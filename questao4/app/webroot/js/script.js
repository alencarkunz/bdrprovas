$( function() {
	$( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
		update: function( event, ui ) { 
			//alert( $('#draganddrop').serialize() ); 
			$.post(URLBASE+'tarefas/serialize', $('#draganddrop').serialize() ,function(data) { 
				if(data.ok) window.location.reload();
			}, 'json');
		}
    });
	$( "#sortable" ).disableSelection();
});
