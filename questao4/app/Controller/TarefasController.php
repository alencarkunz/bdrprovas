<?php

class TarefasController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash');
    public $name = 'Tarefas';

    function index() {
        $this->set('tarefas', $this->Tarefa->find('all', 
            array('order' => array('Tarefa.prioridade asc'))
        ));
    }

    public function view($id = null) {
        $this->set('tarefa', $this->Tarefa->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            if ($this->Tarefa->save($this->request->data)) {
                $this->Flash->success('Salvo com sucesso!');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    function edit($id = null) {
        $this->Tarefa->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Tarefa->findById($id);
        } else {
            if ($this->Tarefa->save($this->request->data)) {
                $this->Flash->success('Alterado com sucesso.');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    function delete($id) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Tarefa->delete($id)) {
            $this->Flash->success('Tarefa id: ' . $id . ' foi deletado.');
            $this->redirect(array('action' => 'index'));
        }
    }

    function serialize() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            foreach ($_POST['list_id'] as $key => $value) {
                $data = array(
                    'id' => $value,
                    'prioridade' => ($key + 1),
                );
                $this->Tarefa->save($data);
            }
            echo json_encode(array('ok' => true));
        }
    }

}
