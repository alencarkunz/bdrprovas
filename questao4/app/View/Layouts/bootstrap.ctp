<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php echo $title_for_layout; ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php
        echo $this->Html->meta('icon');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        
        echo $this->Html->css('style');
    ?>        
    
    <?php
        echo $this->Html->css(array('bootstrap/dist/css/bootstrap.min'));
        echo $this->Html->css(array('bootstrap/dist/css/bootstrap-theme.min'));
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <?php
        echo $this->Html->script('bootstrap/dist/js/bootstrap.min');
    ?>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<script>URLBASE = '<?php echo $this->webroot; ?>'</script>
	<?php
        echo $this->Html->script('script');
    ?> 
    
</head>

<body>

    <?php echo $this->Element('navigation'); ?>

    <div class="container">
        <?php echo $this->Flash->render(); ?>
        
        <?php echo $this->fetch('content'); ?>
    </div><!-- /.container -->

  </body>
</html>
