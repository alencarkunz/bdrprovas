<h1>Nova Tarefa</h1>
<?php
echo $this->Form->create('Tarefa');
echo $this->Form->input('titulo', array('class' => 'form-control', 'required'=> 'required'));
echo $this->Form->input('descricao', array('rows' => '3', 'class' => 'form-control', 'required'=> 'required'));
echo $this->Form->input('prioridade', array('class' => 'form-control', 'required'=> 'required'));
echo $this->Form->submit('Salvar', array(
    'before' => '<hr>',
    'class' => 'btn btn-success btn-lg',
    'after' => ' &nbsp '.$this->Html->link('Voltar', array('action' => 'index'),['class' => 'btn btn-danger btn-lg'])
));

