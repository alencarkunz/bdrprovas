<h1>Tarefas</h1>
<p><?php echo $this->Html->link('Nova Tarefa', array('controller' => 'tarefas', 'action' => 'add'),['class' => 'btn btn-success']); ?></p>
<div class="table-responsive">
	<form id="draganddrop">
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Título</th>
            <th>Descrição</th>
            <th>Prioridade</th>
            <th></th>
        </tr>
        <tbody id="sortable">
        <?php foreach ($tarefas as $tarefa): ?>
            <tr>
                <td><input type="hidden" name="list_id[]" value="<?php echo $tarefa['Tarefa']['id']; ?>"><?php echo $tarefa['Tarefa']['id']; ?></td>
                <td><?php echo $tarefa['Tarefa']['titulo']; ?></td>
                <td><?php echo $tarefa['Tarefa']['descricao']; ?></td>
                <td><?php echo $tarefa['Tarefa']['prioridade']; ?></td>
                <td>
                <?php echo $this->Html->link('Ver',array('controller' => 'tarefas', 'action' => 'view', $tarefa['Tarefa']['id']),['class' => 'btn btn-primary']); ?>
                <?php echo $this->Html->link('Editar', array('action' => 'edit', $tarefa['Tarefa']['id']),['class' => 'btn btn-success']);?>
                <?php echo $this->Form->postLink(
                    'Deletar',
                    array('action' => 'delete', $tarefa['Tarefa']['id']),
                    array('class' => 'btn btn-danger'),
                    'Deseja deletar: '.$tarefa['Tarefa']['titulo'].'?');
                ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	</form>
</div>
