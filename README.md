AVALIAÇÃO

- Questão 1
Criado o arquivo questao1.php e está localizado na raiz.

- Questão 2
Criado o arquivo questao2.php e está localizado na raiz.

- Questão 3
Criado o arquivo questao3.php e está localizado na raiz.

- Questão 4
Criado pasta questao4 e está localizado na raiz. 
Utilizado o framework CakePhp na versão 2.9.1 para o desenvolvimento da API Rest,
sendo possível gerenciar as tarefas(inclusão/alteração/exclusão).
A listagem de tarefas são ordenadas por prioridade crescente, iniciando por 
1(maior prioridade).
Criado os seguintes arquivos:
- Controller TarefasController.php(questao4/app/Controller):
    - Método index: Listagem de todas as tarefas;
    - Método add: Inserção de nova tarefa; 
    - Método edit: Alteração de tarefa;
    - Método delete: Deletar tarefa;
    - Método serialize: Reordenação das tarefas; 
- Model TarefaModel.php para interagir com tabela tarefas do banco de dados(questao4/app/Model).
- View index.ctp, add.ctp, edit.ctp e view.ctp(questao4/app/View/Tarefas/):
  - View index: Listagem de todas as tarefas;
  - View add: Formulário para inserção de nova tarefa;
  - View edit: Formulário para alteração de tarefa;
  - View view: Mostra conteúdo de uma tarefa;

Configuração da rota de acesso no arquivo routes.php(questao4/app/Config/).

Utilizado banco de dados Mysql, criado uma base de dados chamada "test",
 usuário "root" e senha em branco. Criado um indíce para o atributo prioridade
pelo motivo de performace já que o sistema irá utilizar esse atributo para 
ordenação.

Configuração de acesso ao banco de dados no arquivo database.php(questao4/app/Config/)

Abaixo listo o sql para criação do banco de dados:

--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tarefas`
--

CREATE TABLE `tarefas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `descricao` text NOT NULL,
  `prioridade` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tarefas`
--

INSERT INTO `tarefas` (`id`, `titulo`, `descricao`, `prioridade`) VALUES
(1, 'teste', 'teste teste teste', 2),
(2, 'teste 2', 'teste 2 teste 2 ', 1),
(3, 'Teste 3', 'Teste 3 Teste 3 Teste 3', 3),
(4, 'Teste 4', 'Teste 4 Teste 4 Teste 4', 4);

-- --------------------------------------------------------

--
-- Indexes for table `tarefas`
--
ALTER TABLE `tarefas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prioridade` (`prioridade`);

--
-- AUTO_INCREMENT for table `tarefas`
--
ALTER TABLE `tarefas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- fim
--

Diferenciais:
- Criado interface amigável para interação e visualização da lista de tarefas;
- Utilizado jquery para a interface drag and drop no grid de listagem de tarefas, 
ao ordenar item no grid é efetuado uma chamada em ajax para alteração de 
prioridade do item no banco de dados;
- Utilizado Bootstrap para a interface responsiva(desktop e mobile);