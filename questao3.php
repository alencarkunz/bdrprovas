<?php
class MyUserClass
{
    private $localhost = 'localhost';
    private $user = 'root';
    private $password = '';
	
    public function getUserList()
    {
        $dbconn = new DatabaseConnection($this->localhost,$this->user,$this->password);
        $results = $dbconn->query('select name from user order by name asc');
		
        return $results;
    }
}
